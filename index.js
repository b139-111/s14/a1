// First Act

let fName = `Keanu`;
let lName = `Orig`;
let age = 26;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

let workAddress ={
	houseNumber: `32`,
	street: `Washinton`,
	city: `Lincoln`,
	state: `Nebraska`,
}
console.log(`First name: ${fName}`);
console.log(`Last name: ${lName}`);
console.log(`Age: ${age}`);
console.log(`Hobbies:`, hobbies);
console.log(`Work Address:`, workAddress);

// Second Act

function printUserInfo(fName1, lName1, age1){
	console.log(`${fName1} ${lName1} is ${age1} years of age.`);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(hobbies);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(workAddress);

}

printUserInfo("Keanu", `Orig`, 26);

// Third Act

function isMarried(x){
	return(`The value of isMarried is: ${x}`)
}

console.log(isMarried(true));